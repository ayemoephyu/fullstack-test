var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
const sql = require("./db.js");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function (req, res) {
   res.send('Hello World');
   console.log("Hit Get Request!")
})

// This responds a POST request for the homepage
app.post('/',  (req, res) => {
   var temp_data =  JSON.stringify(req.body)
   var temp_var = JSON.parse(temp_data)
  
   // Check MYSQL
   console.log(" Data: " + temp_var.email + " , " + temp_var.password)
})


// This responds a POST request for the homepage
app.post('/newuser',  (req, res) => {
   var temp_data =  JSON.stringify(req.body)
   var temp_var = JSON.parse(temp_data)
  
   // Check MYSQL
   console.log(" Data for New User: " + temp_var.email + " , " + temp_var.password + " , " + temp_var.firstname + " , " + temp_var.lastname);

   sql.query("INSERT INTO Users(firstName,lastName,email,password,confirm_password) values ('" + temp_var.firstname + "','" + temp_var.lastname + "','" + temp_var.email + "','" + temp_var.password + "','" + temp_var.password + "')")
})

// ********************************   ORGANIZATION  ******************************************************
// Retrieve Organization List
app.get('/retrieve_organization',  (req, res) => {
   console.log("Retrieve Organization!!!")
   sql.query("SELECT * FROM organization", function (err, result, fields) {
      if (err) throw err;
      //console.log(result)
      res.send(result)
   });
})
// Retrieve Organization By ID
app.get('/retrieve_organization_by_id/:id',  (req, res) => {
   console.log("Retrieve Organization By ID!!!")
   var org_id = req.params.id
   sql.query("SELECT * FROM organization Where id = " + org_id, function (err, result, fields) {
      if (err) throw err;
      //console.log(result)
      res.send(result)
   });
})
// Add Organization List 
app.post('/add_organization',  (req, res) => {
   var temp_data =  JSON.stringify(req.body)
   var temp_var = JSON.parse(temp_data)
  
   sql.query("INSERT INTO organization(name,owner,address,city,state,country,founded_date) values ('" + temp_var.org_name + "','" + temp_var.owner_name + "','" + temp_var.address + "','" + temp_var.city + "','" + temp_var.state + "','" + temp_var.country + "','" + temp_var.org_founded_date + "')")

   console.log("New Organization added successfully!")
})
// Delete Organization List
app.delete('/delete_organization/:id',  (req, res) => {
   const org_id = req.params.id;
   sql.query("Delete From Organization Where id = " + org_id);
   console.log("Organization is successfully deleted!")
})
// Edit Organization
app.put('/edit_organization/:id', (req,res) => {
   const org_id = req.params.id;
   sql.query("Update organization set name='"+ req.body.org_name + "', owner='" + req.body.owner + "', address = '" +req.body.address + "',city='" + req.body.city + "',state='" + req.body.state + "',country='" + req.body.country + "' where id=" + org_id,function (err, result, fields) {
      if (err) throw err;
      console.log(result)
      res.send(result)
   });
   console.log("Organization is updated successfully!")
})


// ********************************   DEPARTMENT  ******************************************************
// Retrieve Organization List
app.get('/retrieve_department',  (req, res) => {
   console.log("Retrieve Department!!!")
   sql.query("select dep.id,dep.name,dep.description,dep.owner,dep.working_time,dep.working_day,dep.founded_date,org.name as org_name from department dep, organization org where dep.organization_id=org.id;", function (err, result, fields) {
      if (err) throw err;
      console.log(result)
      res.send(result)
   });
})
// Add Department List 
app.post('/add_department',  (req, res) => {
   var temp_data =  JSON.stringify(req.body)
   var temp_var = JSON.parse(temp_data)
  
   sql.query("INSERT INTO department(name,description,owner,working_time,working_day,organization_id,founded_date) values ('" + temp_var.department_name + "','" + temp_var.description + "','" + temp_var.owner + "','" + temp_var.workingtime + "','" + temp_var.workingday + "'," + temp_var.organization_id + ",'" + temp_var.founded_date + "')")

   console.log("New Department added successfully!")
})
// Delete Department 
app.delete('/delete_department/:id',  (req, res) => {
   const dept_id = req.params.id;
   sql.query("Delete From department Where id = " + dept_id);
   console.log("Department is successfully deleted!")
})
// Retrieve Department By ID
app.get('/retrieve_department_by_id/:id',  (req, res) => {
   console.log("Retrieve Department By ID!!!")
   var org_id = req.params.id
   sql.query("SELECT * FROM department Where id = " + org_id, function (err, result, fields) {
      if (err) throw err;
      //console.log(result)
      res.send(result)
   });
})
// Retrieve Department By Organization ID
app.get('/retrieve_department_by_org_id/:id',  (req, res) => {
   console.log("Retrieve Department By Organization's ID!!!")
   var org_id = req.params.id
   sql.query("SELECT * FROM department Where organization_id = " + org_id, function (err, result, fields) {
      if (err) throw err;
      //console.log(result)
      res.send(result)
   });
})
// Edit Department
app.put('/edit_department/:id', (req,res) => {
   const dept_id = req.params.id;
   sql.query("Update department set name='"+ req.body.department_name + "', description='" + req.body.description + "', owner='" + req.body.owner + "', working_time = '" +req.body.workingtime + "',working_day='" + req.body.workingday + "',organization_id=" + req.body.organization_id + " where id =" + dept_id ,function (err, result, fields) {
      if (err) throw err;
      console.log(result)
      res.send(result)
   });
   console.log("Department is updated successfully!")
})

// ********************************   EMPLOYEE  ******************************************************
// Retrieve Employee List
app.get('/retrieve_employee',  (req, res) => {
   console.log("Retrieve Employee!!!")
   sql.query("select emp.id,emp.firstname,emp.lastname,emp.dateofbirth,emp.worktitle,emp.totalexperience,dep.name as depart_name from employee emp, department dep where emp.department_id=dep.id", function (err, result, fields) {
      if (err) throw err;
      console.log(result)
      res.send(result)
   });
})
// Add Employee List 
app.post('/add_employee',  (req, res) => {
   var temp_data =  JSON.stringify(req.body)
   var temp_var = JSON.parse(temp_data)
  
   sql.query("INSERT INTO employee(firstname,lastname,dateofbirth,worktitle,totalexperience,start_date,department_id) values ('" + temp_var.firstname + "','" + temp_var.lastname + "','" + temp_var.dateofbirth + "','" + temp_var.worktitle + "','" + temp_var.totalexperience + "','" + temp_var.startdate + "'," + temp_var.dept_id + ")")

   console.log("New Employee added successfully!")
})
// Delete Employee 
app.delete('/delete_employee/:id',  (req, res) => {
   const empid_id = req.params.id;
   sql.query("Delete From employee Where id = " + empid_id);
   console.log("Employee is successfully deleted!")
})
// Retrieve Employee By ID
app.get('/retrieve_employee_by_emp_id/:id',  (req, res) => {
   console.log("Retrieve Employee By ID!!!")
   var emp_id = req.params.id
   sql.query("SELECT * FROM employee Where id = " + emp_id, function (err, result, fields) {
      if (err) throw err;
      //console.log(result)
      res.send(result)
   });
})
// Edit Employee
app.put('/edit_employee/:id', (req,res) => {
   const emp_id = req.params.id;
   sql.query("Update employee set firstname='"+ req.body.firstname + "', lastname='" + req.body.lastname + "', dateofbirth='" + req.body.dateofbirth + "', worktitle = '" +req.body.worktitle + "',totalexperience='" + req.body.totalexperience + "',department_id=" + req.body.dept_id + " where id =" + emp_id ,function (err, result, fields) {
      if (err) throw err;
      
      res.send(result)
   });
   console.log("Employee is updated successfully!")
})

// Server Start Up
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})