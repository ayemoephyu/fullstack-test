import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component'; 
import { LoginComponent } from './login/login.component';
import { NewuserComponent } from './newuser/newuser.component';

// Organization
import { OrganizationComponent } from './organization/organization.component';
import { OrganizationNewComponent } from './organization-new/organization-new.component'
import { OrganizationEditComponent } from './organization-edit/organization-edit.component'

// Department
import { DepartmentComponent } from './department/department.component';
import { DepartmentNewComponent } from './department-new/department-new.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';

// Employee
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeNewComponent } from './employee-new/employee-new.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';

// Calendar
import { CalendarComponent } from './calendar/calendar.component';


const routes: Routes = [
  {path:"", component: LoginComponent}, // Default Route
  {path:"home", component:HomeComponent}, 
  {path:"login", component:LoginComponent},
  {path:"newuser", component:NewuserComponent},
  {path:"organization",component:OrganizationComponent},
  {path:"neworganization", component: OrganizationNewComponent },
  {path:"editorganization", component: OrganizationEditComponent },
  {path:"department", component: DepartmentComponent },
  {path:"newdepartment", component: DepartmentNewComponent },
  {path:"editdepartment", component: DepartmentEditComponent },
  {path:"employee", component: EmployeeComponent },
  {path:"newemployee", component: EmployeeNewComponent },
  {path:"editemployee", component: EmployeeEditComponent },
  {path:"calendar", component: CalendarComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } export const 
RoutingComponent = [HomeComponent,LoginComponent,NewuserComponent,OrganizationComponent,OrganizationNewComponent,OrganizationEditComponent,DepartmentComponent,DepartmentNewComponent,DepartmentEditComponent,EmployeeComponent,EmployeeNewComponent,EmployeeEditComponent,CalendarComponent];
