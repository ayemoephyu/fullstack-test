import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule,RoutingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MyserviceService } from './myservice.service';
import { HttpClientModule } from '@angular/common/http';
import { OrganizationComponent } from './organization/organization.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { OrganizationNewComponent } from './organization-new/organization-new.component';
 
import { ToastrModule } from 'ngx-toastr';
import { OrganizationEditComponent } from './organization-edit/organization-edit.component';
import { DepartmentComponent } from './department/department.component';
import { DepartmentNewComponent } from './department-new/department-new.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeNewComponent } from './employee-new/employee-new.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponent,
    OrganizationComponent,
    OrganizationNewComponent,
    OrganizationEditComponent,
    DepartmentComponent,
    DepartmentNewComponent,
    DepartmentEditComponent,
    EmployeeComponent,
    EmployeeNewComponent,
    EmployeeEditComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ToastrModule.forRoot(),
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }), // ToastrModule added
  ],
  providers: [MyserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
