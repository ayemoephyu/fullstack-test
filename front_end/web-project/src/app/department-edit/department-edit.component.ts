import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {

  public target_dept;
  public organization_list;
  public dept_id;

  constructor(private myservice: MyserviceService) {
    this.dept_id = localStorage.getItem('dep_id_edit');
    this.RetrieveDepartmentByID(this.dept_id);

    // Retrieve Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
      this.organization_list  = data
 },
      (error) => {
       console.log('error is ', error)
   })

   }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  RetrieveDepartmentByID(dep_id) {
    this.myservice.Retrieve_Department_By_Id(dep_id).subscribe((data)=>{
      this.target_dept =  data;
    });
  }

  
  async Update_Department(data){
    var org_id;

    if(data.org_name === "") {
      data.org_name = this.organization_list[0].name
    }

    this.organization_list.forEach( await function(value){
      if(value.name === data.org_name) {
          org_id = value.id
      }
    })

    var department = {
      id: this.dept_id,
      department_name: data.departmentname,
      description: data.description,
      owner: data.owner,
      workingtime: data.workingtime,
      workingday: data.workingday,
      organization_id: org_id
    }
    this.myservice.Edit_Department(department).subscribe((data)=>{
      window.location.href = 'http://127.0.0.1:4200/department'
    
    })
    
  }
}
