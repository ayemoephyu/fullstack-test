import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-department-new',
  templateUrl: './department-new.component.html',
  styleUrls: ['./department-new.component.css']
})
export class DepartmentNewComponent implements OnInit {

  public organization_list;

  constructor(private myservice: MyserviceService,private toastr: ToastrService) { 
    // Retrieve Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
      this.organization_list  = data
 },
      (error) => {
       console.log('error is ', error)
   })
  }

  ngOnInit(): void {
  }
  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');
  
  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  
  // Add New Department
  async onClickSubmit(data) {
    var org_id;
    var org_founded_date =  new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()  + ", " + new Date().getDay() + "/" + new Date().getMonth() + "/" + new Date().getFullYear()
    
    if(data.org_name === "") {
      data.org_name = this.organization_list[0].name
    }

    this.organization_list.forEach( await function(value){
      if(value.name === data.org_name) {
          org_id = value.id
      }
    })

    var department = {
      department_name: data.departmentname,
      description: data.description,
      owner: data.owner,
      workingtime: data.workingtime,
      workingday: data.workingday,
      organization_id: org_id,
      founded_date: org_founded_date
    }
    
    
    // Check Validation
    if(department.department_name === "" || department.description === "" || department.owner === "" || department.workingtime === "" || department.workingday === ""){
      this.toastr.error('Empty!', 'Check yours!',{
        timeOut: 3000,
        progressBar: true,
      });
    } else {
      this.myservice.Add_Department(department).subscribe((data) => {
      });
      this.toastr.success('Congratulation!', 'New Department Added!',{
        timeOut: 3000,
        progressBar: true,
      });
      window.location.href = 'http://127.0.0.1:4200/department';
    } 
 }

}
