import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  

  ngOnInit(): void {
  }
  public department_list;

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');
  
  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }
  constructor(private myservice: MyserviceService) { 
    
    // Retrieve Department List
    this.myservice.Retrieve_Department().subscribe((data) => {
      this.department_list  = data
    },
      (error) => {
       console.log('error is ', error)
    })
    
  
  }


  
 Route_to_Department_Edit_Component(dep_id) {
  localStorage.setItem('dep_id_edit',dep_id);
  window.location.href = 'http://127.0.0.1:4200/editdepartment';
}

  AddDepartment() {
    window.location.href = 'http://127.0.0.1:4200/newdepartment';
  }

  DeleteDepartment(dep_id) {
    this.myservice.Delete_Department(dep_id).subscribe((data) => {
    });
    window.location.href = 'http://127.0.0.1:4200/department'
  }
}
