import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  public target_emp;
  public target_emp_id;
  public organization_list;
  public department_list;
  public chosen_organization;

  constructor(private myservice: MyserviceService,private toastr: ToastrService) { 
    this.target_emp_id = localStorage.getItem('emp_id_edit');
    this.RetrieveEmployeeByID(this.target_emp_id);

    // Get Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
      this.organization_list  = data
      
      
   // Get Department List
   this.myservice.Retrieve_Department_By_Org_ID(this.organization_list[0].id).subscribe((data) => {
    this.department_list  = data
  },
    (error) => {
     console.log('error is ', error)
  })

    },
      (error) => {
       console.log('error is ', error)
   })

  }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  
  RetrieveEmployeeByID(data) {
    this.myservice.Retrieve_Employee_By_Emp_ID(data).subscribe((data)=>{
      this.target_emp =  data;
    });
  }

  
  async Change_Organization(){
    var chosen_org_id;
    
    await this.organization_list.forEach(value => {
      if(value.name === this.chosen_organization) {
        chosen_org_id = value.id
      }
    });
  
    this.myservice.Retrieve_Department_By_Org_ID(chosen_org_id).subscribe((data) => {
      this.department_list  = data
 },
      (error) => {
       console.log('error is ', error)
   })

   }

  Update_Employee(data){
    var dept_id;

    if(data.dept_name === "") {
      data.dept_name = this.department_list[0].name
    }

    this.department_list.forEach((value)=>{
      if(value.name === data.dept_name) {
        dept_id = value.id;
      }
    })

    var employee = {
      id: this.target_emp_id,
      firstname: data.firstname,
      lastname: data.lastname,
      dateofbirth: data.dateofbirth,
      worktitle: data.worktitle,
      dept_id: dept_id,
      totalexperience: data.totalexperience
    }

    // Check Validation
    if(employee.firstname === "" || employee.lastname === "" || employee.dateofbirth === ""  || employee.worktitle === "" || employee.dept_id === "" || employee.totalexperience === ""){
      this.toastr.error('Empty!', 'Check yours!',{
        timeOut: 3000,
        progressBar: true,
      });
    } else {
      this.myservice.Edit_Employee(employee).subscribe((data) => {
      });
      this.toastr.success('Congratulation!', 'Updated Organization!',{
        timeOut: 3000,
        progressBar: true,
      });
      window.location.href = 'http://127.0.0.1:4200/employee';
    } 
  }
}
