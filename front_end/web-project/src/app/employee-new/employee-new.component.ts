import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-new',
  templateUrl: './employee-new.component.html',
  styleUrls: ['./employee-new.component.css']
})
export class EmployeeNewComponent implements OnInit {

  public organization_list;
  public department_list;

  public chosen_organization;
  constructor(private myservice: MyserviceService,private toastr: ToastrService) {
    
    // Get Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
      this.organization_list  = data
      
      
   // Get Department List
   this.myservice.Retrieve_Department_By_Org_ID(this.organization_list[0].id).subscribe((data) => {
    this.department_list  = data
  },
    (error) => {
     console.log('error is ', error)
  })

    },
      (error) => {
       console.log('error is ', error)
   })

   }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  async Change_Organization(){
    var chosen_org_id;
    
    await this.organization_list.forEach(value => {
      if(value.name === this.chosen_organization) {
        chosen_org_id = value.id
      }
    });
  
    this.myservice.Retrieve_Department_By_Org_ID(chosen_org_id).subscribe((data) => {
      this.department_list  = data
 },
      (error) => {
       console.log('error is ', error)
   })

   }
  
  // Add New Organization
  onClickSubmit(data) {
   var dept_id;
   var employee_start_date =  new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()  + ", " + new Date().getDay() + "/" + new Date().getMonth() + "/" + new Date().getFullYear()
    
    this.department_list.forEach((value)=>{
      if(value.name === data.dept_name) {
        dept_id = value.id;
      }
    })

    var employee = {
      firstname: data.firstname,
      lastname: data.lastname,
      dateofbirth: data.dateofbirth,
      worktitle: data.worktitle,
      dept_id: dept_id,
      totalexperience: data.totalexperience,
      startdate: employee_start_date
    }

    // Check Validation
    if(employee.firstname === "" || employee.lastname === "" || employee.dateofbirth === ""  || employee.worktitle === "" || employee.dept_id === "" || employee.totalexperience === ""){
      this.toastr.error('Empty!', 'Check yours!',{
        timeOut: 3000,
        progressBar: true,
      });
    } else {
      this.myservice.Add_Employee(employee).subscribe((data) => {
      });
      this.toastr.success('Congratulation!', 'New Organization Added!',{
        timeOut: 3000,
        progressBar: true,
      });
      window.location.href = 'http://127.0.0.1:4200/employee';
    } 
 }

}
