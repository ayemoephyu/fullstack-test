import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employee_list;

  constructor(private myservice: MyserviceService) {
    // Retrieve Department List
    this.myservice.Retrieve_Employee().subscribe((data) => {
      this.employee_list  = data
    },
      (error) => {
       console.log('error is ', error)
    })
   }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  AddEmployee(){
    window.location.href = 'http://127.0.0.1:4200/newemployee';
  }

  DeleteEmployee(emp_id){
    if(confirm("Do you want to delete!")) {
      this.myservice.Delete_Employee(emp_id).subscribe((data)=>{})
      window.location.href = 'http://127.0.0.1:4200/employee';
    } else {
    }
  }

  Route_to_Employee_Edit_Component(emp_id) {
    localStorage.setItem('emp_id_edit',emp_id);
    window.location.href = 'http://127.0.0.1:4200/editemployee';
  }

}
