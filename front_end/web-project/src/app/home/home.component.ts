import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  

  ngOnInit(): void {
  }
  
  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  public organization_list;
  public department_list;
  public employee_list;

  constructor(private myservice: MyserviceService) {
    // Get Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
      this.organization_list  = data
    },
      (error) => {
       console.log('error is ', error)
   })
  
   // Get Department List
   this.myservice.Retrieve_Department().subscribe((data) => {
    this.department_list  = data
  },
    (error) => {
     console.log('error is ', error)
  })

  // Get Employee List
  this.myservice.Retrieve_Employee().subscribe((data) => {
    this.employee_list  = data
  },
    (error) => {
     console.log('error is ', error)
  })

   }
}
