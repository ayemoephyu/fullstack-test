import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private myservice: MyserviceService) { }

  ngOnInit(): void {
  }

  onClickSubmit(data) {
    if(data.emailid === "" ) {
      alert("Empty Email!"); 
    }
    else {
      if(this.ValidateEmail(data.emailid)) {
         var user = { email: data.emailid, password: data.passwd};

        this.myservice.SendData(user).subscribe((data) => {
          
       });

       // Store Session Object
       localStorage.setItem('loggedIn','true');

       window.location.href = 'http://127.0.0.1:4200/home';
       

      } else {
        alert("Wrong Email Format!")
      }
    }
 }

ValidateEmail(inputText)
{
var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
if(inputText.match(mailformat))
{
return true;
}
else
{
return false;
}
}
}
