import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
  private apiurl = "http://localhost:8081/";
   
  constructor(private httpClient: HttpClient) { }

  SendData(data) {
    var user = { 'email': data.email, 'password': data.password}
    return this.httpClient.post(this.apiurl,user)
 }

 NewUser(data) {
  var user = { 'firstname':data.firstname,'lastname':data.lastname,'email': data.email, 'password': data.password}
  return this.httpClient.post("http://localhost:8081/newuser",user)
 }

 // ORGANIZATIONS
 Retrieve_Organization() {
   return this.httpClient.get("http://localhost:8081/retrieve_organization")
 }
 // Retrieve Organization with ID
 Retrieve_Organization_By_Id(org_id) {
  return this.httpClient.get("http://localhost:8081/retrieve_organization_by_id/"+org_id)
 }
 Add_Organization(organization) {
    return this.httpClient.post("http://localhost:8081/add_organization",organization)
 }
 Delete_Organization(organization_id)
 {
   return this.httpClient.delete("http://localhost:8081/delete_organization/"+organization_id);
 }
Edit_Organization(organization) 
{
  return this.httpClient.put("http://localhost:8081/edit_organization/" + organization.id,organization)
}

// Department
Retrieve_Department() {
  return this.httpClient.get("http://localhost:8081/retrieve_department")
}
// Retrieve Department with ID
Retrieve_Department_By_Id(dep_id) {
  return this.httpClient.get("http://localhost:8081/retrieve_department_by_id/" + dep_id)
 }
// Retrieve Department with Organization's ID
Retrieve_Department_By_Org_ID(org_id) {
  return this.httpClient.get("http://localhost:8081/retrieve_department_by_org_id/" + org_id)
}
Add_Department(department) {
   return this.httpClient.post("http://localhost:8081/add_department",department)
}
Delete_Department(dept_id)
{
  return this.httpClient.delete("http://localhost:8081/delete_department/"+dept_id);
}
Edit_Department(department) 
{
  return this.httpClient.put("http://localhost:8081/edit_department/" + department.id,department)
}

// Employee
Retrieve_Employee() {
  return this.httpClient.get("http://localhost:8081/retrieve_employee")
}
Retrieve_Employee_By_Emp_ID(emp_id) {
  return this.httpClient.get("http://localhost:8081/retrieve_employee_by_emp_id/" + emp_id)
}
Add_Employee(employee) {
  return this.httpClient.post("http://localhost:8081/add_employee",employee)
}
Delete_Employee(employee_id)
{
  return this.httpClient.delete("http://localhost:8081/delete_employee/" + employee_id);
}
Edit_Employee(employee) 
{
  return this.httpClient.put("http://localhost:8081/edit_employee/" + employee.id,employee)
}

}
