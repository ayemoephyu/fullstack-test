import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent implements OnInit {

  constructor(private myservice: MyserviceService) { }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }
  onClickSubmit(data) {
    if(data.emailid === "" ) {
      alert("Empty Email!"); 
    }
    else {
      if(this.ValidateEmail(data.emailid) && this.ValidatePassword(data.passwd,data.confirmpasswd)) {
        alert("FirstName: " + data.firstname + " , LastName: " + data.lastname + ", Email: " + data.emailid + " , password: " + data.passwd + " , confirm_password: " + data.confirmpasswd);
        
        var user = {firstname: data.firstname, lastname: data.lastname, email: data.emailid, password: data.passwd};

        this.myservice.NewUser(user).subscribe((data) => {
          
       });
      } 
    }
 }

ValidatePassword(password,confirm_password) {
  if(password === confirm_password) {
    return true;
  } else {
    alert("Error" + "\n" + "Password Mismatch!");
    return false;
  }
}

ValidateEmail(inputText)
{
var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
if(inputText.match(mailformat))
{
return true;
}
else
{
return false;
}
}
}
