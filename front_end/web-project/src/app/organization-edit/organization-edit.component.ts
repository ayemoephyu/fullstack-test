import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-organization-edit',
  templateUrl: './organization-edit.component.html',
  styleUrls: ['./organization-edit.component.css']
})
export class OrganizationEditComponent implements OnInit {

  public target_org;

  constructor(private myservice: MyserviceService) {
    var org_id = localStorage.getItem('org_id_edit');
    this.RetrieveOrganizationByID(org_id);
    ///localStorage.removeItem('org_id_edit');
  }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }

  RetrieveOrganizationByID(data) {
    this.myservice.Retrieve_Organization_By_Id(data).subscribe((data)=>{
      this.target_org =  data;
    });
  }

  async Update_Organization(data){
    var organization = {
      id: data.organizationid,
      org_name: data.organizationname,
      owner: data.owner,
      address: data.address,
      city: data.city,
      state: data.state,
      country: data.country
    }

    this.myservice.Edit_Organization(organization).subscribe((data)=>{
      window.location.href = 'http://127.0.0.1:4200/organization'
    
    })
    
  }

}
