import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-organization-new',
  templateUrl: './organization-new.component.html',
  styleUrls: ['./organization-new.component.css']
})
export class OrganizationNewComponent implements OnInit {

  constructor(private myservice: MyserviceService,private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

  // Add New Organization
  onClickSubmit(data) {
    var org_start_date =  new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()  + ", " + new Date().getDay() + "/" + new Date().getMonth() + "/" + new Date().getFullYear()
    
    var organization = {
      org_name: data.organizationname,
      owner_name: data.owner,
      address: data.address,
      city: data.city,
      state: data.state,
      country: data.country,
      org_founded_date: org_start_date
    }
    
    // Check Validation
    if(organization.org_name === "" || organization.owner_name === "" || organization.address === "" || organization.city === "" || organization.state === "" || organization.country === ""){
      this.toastr.error('Empty!', 'Check yours!',{
        timeOut: 3000,
        progressBar: true,
      });
    } else {
      this.myservice.Add_Organization(organization).subscribe((data) => {
      });
      this.toastr.success('Congratulation!', 'New Organization Added!',{
        timeOut: 3000,
        progressBar: true,
      });
      window.location.href = 'http://127.0.0.1:4200/organization';
    } 
 }

 
}
