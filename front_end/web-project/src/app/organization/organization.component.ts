import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  public organization_list;

  constructor(private myservice: MyserviceService,private modalService: NgbModal) {
    console.log('Organization Constructor') 
    
    // Retrieve Organization List
    this.myservice.Retrieve_Organization().subscribe((data) => {
           this.organization_list  = data
      },
           (error) => {
            console.log('error is ', error)
        })
  }

  ngOnInit(): void {
  }

  loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  LogOut(event) {
    localStorage.setItem('loggedIn','false')
    window.location.href = 'http://127.0.0.1:4200/login';
  }
  
  // Retrieve Organization By ID
  Route_to_Organization_Edit_Component(org_id) {
    localStorage.setItem('org_id_edit',org_id);
    window.location.href = 'http://127.0.0.1:4200/editorganization';
  }

  // Add Organization
  AddOrganization() {
      window.location.href = 'http://127.0.0.1:4200/neworganization';
  }
  
  // Edit Organization
 EditOrganization(data) {
  this.myservice.Edit_Organization(data).subscribe((data) => {
  });
  }

  // Delete Organization 
  DeleteOrganization(data) {
    this.myservice.Delete_Organization(data).subscribe((data) => {
    });
    window.location.href = 'http://127.0.0.1:4200/organization'
  }

}
